import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=DeprecationWarning)
warnings.simplefilter(action='ignore', category=UserWarning)

from keras.backend.tensorflow_backend import set_session
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU

sess = tf.Session(config=config)

set_session(sess)  # set this TensorFlow session as the default session for Keras

import sys
import cv2
import sys
import numpy as np
from annoy import *
import collections
import time

from yolo_detector import *
from keras.preprocessing import image
from keras.applications.resnet50 import ResNet50
from keras.applications.vgg16 import VGG16
from keras.applications.mobilenet_v2 import MobileNetV2

from pyod.models.cblof import CBLOF


ONECLF = CBLOF


def detectPersonsYolo(yolo, cv2Frame):
	img_orig = cv2Frame
	h = float(img_orig.shape[0])
	w = float(img_orig.shape[1]) 
	img_orig_rgb = img_orig[...,::-1]

	r = yolo.detect(img_orig_rgb)

	personsAOI = []

	for reg in r:
		xmin = int(reg[2][0]-(reg[2][2]/2))
		xmin = int(float(xmin)/w*cv2Frame.shape[1])
		if xmin < 0: xmin = 0
		ymin = int(reg[2][1]-(reg[2][3]/2))
		ymin = int(float(ymin)/h*cv2Frame.shape[0])
		if ymin < 0: ymin = 0
		xmax = int(xmin + reg[2][2])
		xmax = int(float(xmax)/w*cv2Frame.shape[1])
		if xmax > cv2Frame.shape[1]: xmax = cv2Frame.shape[1]
		ymax = int(ymin + reg[2][3])
		ymax = int(float(ymax)/h*cv2Frame.shape[0])
		if ymax > cv2Frame.shape[0]: ymax = cv2Frame.shape[0]

		if reg[0] == 'person':
			personsAOI.append([xmin,ymin,xmax,ymax,False])

	return personsAOI


def readOURDataset():

	trainVids = ["test_videos/train-felix.mp4",
			 	"test_videos/train-miguel.mp4",
			 	"test_videos/train-carlos.mp4",
			 	]

	testVids = ["test_videos/test-felix.mp4",
				"test_videos/test-miguel.mp4",
				"test_videos/test-carlos.mp4",
				]

	classes_train = ["0", "1", "2"]
	classes_test = ["","",""]

	return trainVids, classes_train, testVids, classes_test


def train_kNN(x_train, y_train, n):
	knnTree = AnnoyIndex(n)
	knnClasses = []
	idxSamplesTree = 0
	for x,y in zip(x_train, y_train):
		knnTree.add_item(idxSamplesTree, x)
		idxSamplesTree+=1
		knnClasses.append(y)
	knnTree.build(10) 
	return knnTree, knnClasses


def train_SOS(x_train):
	clf = ONECLF(contamination=0.001)
	clf.fit(x_train)
	return clf

def classify_knn(knnTree, knnClasses, h, k):
	res = knnTree.get_nns_by_vector(h,k,search_k=-1, include_distances=True)
	predictions = []
	for idx in res[0]:
		predictions.append(knnClasses[idx])

	counts = np.unique(np.array(predictions), return_counts=True)
	res = max(set(predictions), key = predictions.count)
	return str(res)

def classify_SOS(sosClf, h):
	res = sosClf.predict(np.array([h]))
	return res[0]



def main():

	trainVidsFlat, classes_train, testVidsFlat, classes_test = readOURDataset()

	feature_extractor = sys.argv[2]

	if feature_extractor == "rn50":
		from keras.applications.resnet50 import preprocess_input
		featureExtractor = ResNet50(weights='imagenet', include_top=False, pooling='avg') 
		n = 2048
	elif feature_extractor == "vgg16":
		from keras.applications.vgg16 import preprocess_input
		featureExtractor = VGG16(weights='imagenet', include_top=False)
		n = 25088
	elif feature_extractor == "mn":
		from keras.applications.mobilenet_v2 import preprocess_input
		featureExtractor = MobileNetV2(weights='imagenet', include_top=False, pooling='avg')
		n = 1280


	yolo = YOLORegionExtraction()


	frameskip = int(sys.argv[1]) 

	visualization = True 
	x_train = []
	y_train = []
	

	print("Now extracting training data from the frames, wait...")

	for v in range(len(trainVidsFlat)):

		cap = cv2.VideoCapture(trainVidsFlat[v])

		frameIdx = 0

		while(cap.isOpened()):
			ret, frame = cap.read()
			if ret == False: break
			frameToDisplay = frame.copy()

			frameIdx += 1

			# enable frame skip
			if frameskip is not None and frameIdx % frameskip != 0:
				continue # '''

			personsAOI = detectPersonsYolo(yolo, frame)

			if len(personsAOI) == 0:
				continue

			refereeAOI = frame[personsAOI[0][1] : personsAOI[0][3] , personsAOI[0][0] : personsAOI[0][2]]
			refereeAOI = cv2.resize(refereeAOI, (224,224))[...,::-1]

			img = image.img_to_array(refereeAOI)
			x = preprocess_input(np.expand_dims(img.copy(), axis=0))
			h = featureExtractor.predict(x)

			h = h.reshape(n)

			x_train.append(h)
			y_train.append(classes_train[v])


			if visualization is True:
				frameToDisplay = cv2.rectangle(np.array(frameToDisplay), (personsAOI[0][0],personsAOI[0][1]),(personsAOI[0][2],personsAOI[0][3]), (255,255,0),4)
				cv2.putText(frameToDisplay, "Now learning: "+str(classes_train[v]), (10,460), cv2.FONT_HERSHEY_DUPLEX, 1.2, (255,0,255), lineType=cv2.LINE_AA)
				frameToDisplay = cv2.resize(frameToDisplay, (640,480))
				cv2.imshow("visualizer", frameToDisplay)
				key = cv2.waitKey(1)
				
		if visualization is True:
			cv2.destroyWindow("ref")


	print("Training data acquired.")
	print("Now training the classifiers, wait...")

	knnTree = None
	start = time.time()
	knnTree, knnClasses = train_kNN(x_train, y_train, n)
	knnTime = time.time()-start
	print("knn training time "+str(knnTime))

	clfSos = None
	start = time.time()
	clfSos = train_SOS(x_train)
	print("SOS training time "+str(time.time()-start))

	print(str(len(knnClasses))+" samples ")
	print("frames per category")
	print(np.unique(knnClasses, return_counts=True))

	print("Model built. Now testing...")

	frameIdx = 0
	frameskip = 10

	for j in range(len(testVidsFlat)):

		print("video "+str(j+1)+" / "+str(len(testVidsFlat)))
		v = testVidsFlat[j]

		cap = cv2.VideoCapture(v) 

		while(cap.isOpened()):

			frameIdx += 1
			if frameskip is not None and frameIdx % frameskip != 0:
				continue # '''

			ret, frame = cap.read()
			if ret == False: break
			frameToDisplay = frame.copy()

			personsAOI = detectPersonsYolo(yolo, frame)

			for i in range(len(personsAOI)):

				p = personsAOI[i]

				personAOI = frame[p[1] : p[3] , p[0] : p[2]]
				personAOI = cv2.resize(personAOI, (224, 224))[...,::-1]

				start_res = time.time()
				img = image.img_to_array(personAOI)
				x = preprocess_input(np.expand_dims(img.copy(), axis=0))
				h = featureExtractor.predict(x)

				h = h.reshape(n)

				res1 = classify_knn(knnTree, knnClasses, h, 1)
				res = classify_SOS(clfSos, h) 

				if visualization is True:

					if res == 1: label = "UNKW"
					if res == 0: label = str(res1)

					frameToDisplay = cv2.rectangle(np.array(frameToDisplay), (p[0],p[1]),(p[2],p[3]), (255,0,0),4)
					cv2.putText(frameToDisplay, label, (p[0],p[1]-5), cv2.FONT_HERSHEY_DUPLEX, 1.4, (255,0,255), lineType=cv2.LINE_AA)


			frameToDisplay = cv2.resize(frameToDisplay, (640,480))
			cv2.imshow("visualizer", frameToDisplay)
			key = cv2.waitKey(10)


if __name__== "__main__":
	main()